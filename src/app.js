/*jshint esversion: 6*/

//import os from 'os'; // native node.js module
import { remote, ipcRenderer } from 'electron'; // native electron module
//import jetpack from 'fs-jetpack'; // module loaded from npm
//import env from './env';
import Task from './tasks/tasks';
import Time from './tasks/time';
import Storage from './tasks/storage';
import { $ } from './helpers/util';

var app = remote.app;
//var appDir = jetpack.cwd(app.getAppPath());

//var editMenu = Menu.buildFromTemplate(editMenuTemplate);

// Holy crap! This is browser window with HTML and stuff, but I can read
// here files like it is node.js! Welcome to Electron world :)
//console.log('The author of this app is:', appDir.read('package.json', 'json').author);

const appTitle = "ClockIn";


document.addEventListener('DOMContentLoaded', function () {

    // Task.add({
    //     title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis efficitur ante id tincidunt ullamcorper.',
    //     spent: '0:17',
    //     estimate: '1 h'
    // });
    //
    // Task.add({
    //     title: 'New task 2',
    //     spent: '0:15',
    //     estimate: '2 h'
    // });

    let data = Storage.load();
    for (let i in data)
        Task.add(data[i]);

    let taskList = new Vue({
        el: '.task-list',
        data: {
            active: -1,
            tasks: Task.list
        },
        methods: {
            start(i) {
                Task.stop(this.active);
                Task.start(i);
                this.tasks[i].active = true;
                this.active = i;

                app.tray.setToolTip(this.tasks[i].title + " - " + appTitle);
            },
            stop() {
                Task.stop(this.active);
                this.active = -1;
                this.update();

                app.tray.setToolTip(appTitle);
            },
            toggle(i) {
                if (this.active == i)
                    this.stop();
                else
                    this.start(i);
            },
            remove(i) {
                if (i === 'all') {
                    for (let i in this.tasks) {
                        Task.remove(i);
                        if (this.active == i)
                            this.stop();
                    }
                }
                else {
                    Task.remove(i);
                    if (this.active == i)
                        this.stop();
                }

                this.update(true);
                Storage.store(Task.list);
            },
            update(force = false) {
                for (let i in this.tasks) {
                    this.tasks[i].spent = Time.format( Task.getSpent(i) );
                }
                if (force)
                    this.$forceUpdate();  // Don't know why this is necessary, but it is.
            },
            setTask(i, data) {
                Task.list[i] = data;
                console.log(this.tasks);
            }
        }
    });

    ipcRenderer.on('edit-task', function(event, task) {
        let editForm = new Vue({
            el: '.edit-form',
            data: task,
            methods: {
                save(event) {
                    event.preventDefault();
                    app.mainWindow.webContents.send('save-task', task);
                    remote.getCurrentWindow().close();
                },
                close() {
                    remote.getCurrentWindow().close();
                }
            },
            created: function() {
                // This is a really, really shitty workaround, but it seems to be
                // a bug. There is no event after which I can focus the box and have it work.
                setTimeout(function() {
                    let el = document.querySelector('[autofocus]');
                    el.focus();
                    el.select();
                }, 100);
            }
        });
    });

    ipcRenderer.on('save-task', function(event, task) {

        if (task.id === -1) {
            // New task
            Task.add(task);
            Storage.store(Task.list);
        } else {
            // Update task
            Task.edit(task.id, task);
            taskList.update(true);
            Storage.store(Task.list);
        }
    });

    setInterval(function() {
        if (taskList.active >= 0)
            taskList.update();
    }, 1000);

    $('.task-list').on('contextmenu', function(event) {
        let Menu = remote.Menu, cm;

        let el = event.target || event.srcElement;
        while (el && ( ! el.getAttribute('class') || el.getAttribute('class').indexOf('task-item') < 0))
            el = el.parentElement;

        if (el) {
            let listIndex = el.getAttribute('index');
            cm = Menu.buildFromTemplate([
                {label: 'Edit', click() {
                    editTask(listIndex);
                }},
                {label: 'Delete', click() {
                    taskList.remove(listIndex);
                }}
            ]);
        }
        else {
            cm = Menu.buildFromTemplate([
                {label: 'New task', click() {
                    editTask(-1);
                }}
            ]);
        }

        cm.popup(remote.getCurrentWindow());
    });

    document.body.on('selectstart', function(event) {
        event.preventDefault();
        return false;
    });

    $('.btn-close').on('click', function() {
        remote.getCurrentWindow().close();
    });

    $('.btn-stop-tasks').on('click', function() {
        taskList.stop();
    });

    $('.btn-new-task').on('click', function() {
        editTask(-1);
    });

    $('.btn-reset-tasks').on('click', function() {
        let dialog = remote.dialog;
        dialog.showMessageBox({ type: 'warning', buttons: ['OK', 'Cancel'], defaultId: 0, message: "Are you sure?" }, function(rv) {
            if (rv === 0) {
                // TODO
            }
        });
    });

    $('.btn-clear-tasks').on('click', function() {
        let dialog = remote.dialog;
        dialog.showMessageBox({ type: 'warning', buttons: ['OK', 'Cancel'], defaultId: 0, message: "Are you sure?" }, function(rv) {
            if (rv === 0) {
                taskList.remove('all');
            }
        });
    });


    let editTask = function(i) {
        let BrowserWindow = remote.BrowserWindow;
        let editWindow = new BrowserWindow({
            parent: app.mainWindow,
            modal: true,
            frame: false,
            show: false,
            width: 400,
            height: 200,
            resizable: false
        });

        editWindow.setTitle("Fish");
        editWindow.loadURL('file://' + __dirname + '/edit-task.html');
        editWindow.once('ready-to-show', () => {
            editWindow.show();
            let task;
            if (i === -1)
                task = {
                    title: '',
                    spent: '',
                    estimate: ''
                    //adjustment: ''
                };
            else
                task = taskList.tasks[i];
            task.id = i;
            editWindow.webContents.send('edit-task', task);
        });
    };

    app.mainWindow.on('close', function() {
        Storage.store(Task.list);
    });

    taskList.update();
});
