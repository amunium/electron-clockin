/*jshint esversion: 6*/
// This is main process of Electron, started as first thing when your
// app starts. This script is running through entire life of your application.
// It doesn't have any windows which you can see on screen, but we can open
// window from here.

import { app, Menu, Tray, dialog } from 'electron';
import { devMenuTemplate } from './menu/dev_menu_template';
import { editMenuTemplate } from './menu/edit_menu_template';
import { trayMenuTemplate } from './menu/tray_menu_template';
import { contextMenuTemplate } from './menu/context_menu_template';
import createWindow from './helpers/window';

// Special module holding environment variables which you declared
// in config/env_xxx.json file.
import env from './env';

var mainWindow;

var setApplicationMenu = function () {
    var menus = [editMenuTemplate];
    if (env.name !== 'production') {
        menus.push(devMenuTemplate);
    }
    Menu.setApplicationMenu(Menu.buildFromTemplate(menus));
};

// Save userData in separate folders for each environment.
// Thanks to this you can use production and development versions of the app
// on same machine like those are two separate apps.
if (env.name !== 'production') {
    var userDataPath = app.getPath('userData');
    app.setPath('userData', userDataPath + ' (' + env.name + ')');
}

app.on('ready', function () {
    if (env.name !== 'production')
        setApplicationMenu();

    var mainWindow = createWindow('main', {
        width: 1000,
        height: 600,
        frame: false,
        //icon: __dirname + '/icon.ico',
        show: false
    });

    mainWindow.loadURL('file://' + __dirname + '/app.html');

    var tray = new Tray(__dirname + '/resources/tray.ico');
    var trayMenu = Menu.buildFromTemplate(trayMenuTemplate);
    tray.setToolTip("ClockIn");
    tray.setContextMenu(trayMenu);
    tray.on('click', function() {
        mainWindow.show();
    });

    /**
     * Don't show window until content is ready.
     */
    mainWindow.once('ready-to-show', () => {
        mainWindow.show();
    });

    /**
     * Minimize to tray on close
     */
    mainWindow.on('close', function (event) {
        if( ! app.forceQuit){
            event.preventDefault();
            mainWindow.hide();
        }
        return false;
    });


    if (env.name === 'development') {
        mainWindow.openDevTools();
    }

    app.mainWindow = mainWindow;
    app.tray = tray;
});

app.on('window-all-closed', function () {
    app.quit();
});
