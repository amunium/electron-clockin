/*jshint esversion: 6*/
/**
 * Event handler shorthands
 */
NodeList.prototype.on = function(action, func) {
    this.forEach(function(item) {
        item.addEventListener(action, func);
    });
    return this;
};

HTMLElement.prototype.on = function(action, func) {
    this.addEventListener(action, func);
    return this;
};

/**
 * jQuery-like element grabber shorthand
 */
export var $ = function(query) {
    return document.querySelectorAll(query);
};
