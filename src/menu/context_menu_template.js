/*jshint esversion: 6*/
import Task from '../tasks/tasks';

export var contextMenuTemplate = [
    {label: 'Edit'},
    {label: 'Delete', click() {
        console.log('delete');
    }}
];
