/*jshint esversion: 6*/
import { app } from 'electron';

export var trayMenuTemplate = [
    {label: 'Show window', click() {
        app.mainWindow.show();
    }},
    {label: 'Exit', click() {
        app.forceQuit = true;
        app.quit();
    }}
];
