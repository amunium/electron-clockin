/*jshint esversion: 6*/
import jetpack from 'fs-jetpack';
import Time from '../tasks/time';
import { remote } from 'electron';

var app = remote.app;

export default class Storage {

    static store(data) {
        console.log('Saving', data);

        let cleaned = [];
        for (let i in data) {
            let temp = data[i];
            if ( ! temp.hidden) {
                //delete temp.spent;
                cleaned.push(temp);
            }
        }

        jetpack.cwd(app.getPath('userData')).write('tasks.json', cleaned);
    }

    static load() {
        let data = jetpack.cwd(app.getPath('userData')).read('tasks.json', 'json');

        for (let i in data) {
            for (let j in data[i].times) {
                if (data[i].times[j].start)
                    data[i].times[j].start = new Date(data[i].times[j].start);
                if (data[i].times[j].end)
                    data[i].times[j].end = new Date(data[i].times[j].end);
            }

            //data[i].adjustment = parseInt(data[i].adjustment || 0);
        }
        console.log('Loading', data);
        return data;
    }

}
