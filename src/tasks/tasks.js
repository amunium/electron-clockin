/*jshint esversion: 6*/
let _list = [];
let _active_timer = -1;

export default class Task {

    static add(data) {
        if (data.constructor === Array)
            _list.concat(this.formatData(data));
        else
            _list.push(this.formatData(data));
    }

    static remove(i) {
        _list[i].hidden = true;
    }

    static edit(i, data) {
        _list[i] = this.formatData(data);
    }

    /**
     * Start a timer, saving the current Date as the start time.
     */
    static start(i) {
        if ( ! _list[i].times)
            _list[i].times = [];

        let len = _list[i].times.length;

        _list[i].times[len] = {
            start: new Date()
        };

        _active_timer = len;
    }

    /**
     * Stop all timers.
     * If one is active, close it, saving the current Date as end time.
     */
    static stop(i) {
        for (let j in _list) {
            delete _list[j].active;
        }

        if (i !== null && _active_timer >= 0) {
            _list[i].times[_active_timer].end = new Date();
            _list[i].times[_active_timer].diff = this.getDiff(i, _active_timer);
            _active_timer = -1;
        }
    }

    static getDiff(i, timer) {
        let start = _list[i].times[timer].start;
        let end   = _list[i].times[timer].end === undefined ?
            new Date() :
            _list[i].times[timer].end;

        let diff = Math.round((end - start) / 1000);
        _list[i].times[timer].diff = diff;

        return diff;
    }

    static getSpent(i) {
        let times = _list[i].times;
        let total = 0;
        for (let j in times) {
            total += this.getDiff(i, j);
        }
        //total += (_list[i].adjust || 0);
        return total;
    }

    static get list() {
        return _list;
    }

    static formatData(data) {
        let allowed = [
            'title', 'spent', 'estimate', 'times', 'adjustment'
        ];
        for (let f in data) {
            if (allowed.indexOf(f) < 0)
                delete data[f];
        }

        return data;
    }
}
