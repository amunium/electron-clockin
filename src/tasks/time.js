/*jshint esversion: 6*/
export default class Time {
    static format(seconds) {
        let h = Math.floor(seconds / 3600);
        let m = Math.floor((seconds % 3600) / 60);
        let s = seconds % 60;

        return h + ':' + (m < 10 ? '0' + m : m);
    }

    static toSeconds(input) {
        // Assume int is already seconds.
        if (parseInt(input == input))
            return input;
        // Interpret 'hh:mm' format.
        let matches = String(input).match(/^([\d]+):([\d]+)$/);
        if (matches) {
            return (parseInt(matches[1]) * 3600) + (parseInt(matches[2]) + 60);
        }

        return 0;
    }
}
